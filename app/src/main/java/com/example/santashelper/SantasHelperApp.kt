package com.example.santashelper
import android.app.Application
import android.content.Context

class SantasHelperApp : Application() {
    companion object {
        private lateinit var INSTANCE: SantasHelperApp
        val context: Context
            get() = INSTANCE
    }

    init {
        INSTANCE = this
    }
}