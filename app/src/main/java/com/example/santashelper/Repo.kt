package com.example.santashelper

import com.squareup.sqldelight.ColumnAdapter
import com.squareup.sqldelight.EnumColumnAdapter
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import kotlinx.coroutines.flow.Flow

class Repo {
    private val query: AppDbQueries

    init {
        val driver: SqlDriver =
            AndroidSqliteDriver(Database.Schema, SantasHelperApp.context, "dbFile.db")
        val db = Database(
            driver = driver,
            childAdapter = Child.Adapter(
                offencesAdapter = object  : ColumnAdapter<List<String>, String >{
                    override fun decode(databaseValue: String): List<String> {
                        return databaseValue.split(", ")
                    }

                    override fun encode(value: List<String>): String {
                       return value.joinToString(", ")
                    }
                },
                childTypeAdapter = EnumColumnAdapter()
            ),

            giftAdapter = Gift.Adapter(
                typeAdapter = EnumColumnAdapter()
            )
        )
        query = db.appDbQueries
        insertMockData()
    }

    private fun insertMockData() {
        query.clearAllChildren()
        query.clearAllGifts()

        query.transaction {
            insertChild(Child("Radu", 8, ChildType.NAUGHTY, listOf("Nu renunta la tigare")))
            insertChild(Child("Daniela", 4, ChildType.NICE, null))
            insertChild(Child("Mirela", 2, ChildType.NAUGHTY, listOf("Nu e aici s-o vedem pe Chifteluta")))

            insertGift(Gift("carbune", ChildType.NAUGHTY))
            insertGift(Gift("nuia", ChildType.NAUGHTY))
            insertGift(Gift("lego", ChildType.NICE))
        }

    }

    fun insertChild(child: Child) {
        query.insertChildObject(child)
    }

    fun insertGift(gift: Gift) {
        query.insertGift(gift)
    }

    fun retrieveAll(): List<String> {
        return query.selectAllChildren(mapper = { full_name, age, child_type, offences -> "$full_name, $age, $offences => $child_type" })
            .executeAsList()
    }

    fun retriveAllAsFlow() : Flow<List<String>> {
        return query.selectAllChildren(
            mapper = { full_name, age, child_type, offences -> "$full_name, $age, $offences => $child_type" }
        ).asFlow().mapToList()
    }

    fun retrieveChildrenWithGiftOption(childType: ChildType) : Flow<List<String>> {
        return query.getChildrenByGiftOption(
            mapper = {full_name, _, _, _, name, type -> "$full_name, $type => $name" },
            childType = childType
        ).asFlow().mapToList()
    }
}