package com.example.santashelper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private val repo = Repo()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        add_btn.setOnClickListener {
            repo.insertChild(Child("new child", null, ChildType.NAUGHTY, null))
        }

        updateListInRealTime()
    }

    private fun updateListInRealTime() {
        GlobalScope.launch {
            repo.retrieveChildrenWithGiftOption(ChildType.NAUGHTY)
                .map {
                    db_entries_text.text = it.joinToString("\n")
                }
                .flowOn(Dispatchers.Main)
                .collect()
        }

    }

}