package com.example.santashelper

enum class ChildType {
    NAUGHTY, NICE
}